import { Component } from '@angular/core';
import { FabContainer } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Platform } from 'ionic-angular';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public alertCtrl: AlertController, platform: Platform) {
    this.platform = platform;
  }

  platform = null;

  registerCredentials = { email: '', password: '' };

  login() {
    alert("Your email: " + this.registerCredentials.email + "\n" + "Your password: " + this.registerCredentials.password);
  };

  createAccount() {

  };

  getUserInfo() {

  };

  logout() {

  };

  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Sair',
      message: 'De certeza que quer sair ?',
      buttons: [
        {
          text: 'Sim',
          handler: () => {
            this.platform.exitApp();
          }
        },
        {
          text: 'Não',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  share(socialNet: string, fab: FabContainer) {
    fab.close();
    console.log("Sharing in", socialNet);
  }

}
